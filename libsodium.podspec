Pod::Spec.new do |s|
  s.name        = 'libsodium'
  s.version     = '1.0.10'
  s.authors     = { 'Frank Dennis' => 'j@pureftpd.org' }
  s.homepage    = 'https://github.com/jedisct1/libsodium'
  s.summary     = 'Sodium is a portable, cross-compilable, installable, packageable, API-compatible version of NaCl.'
  s.source      = { :git => 'https://bitbucket.org/leeful/libsodium.git',
                    :branch => 'master',
                    :tag => s.version.to_s }
  s.license     = { :type => "BSD",
                    :text => "Copyright © 2013\\nFrank Denis <j at pureftpd dot org>\\n\\nPermission to use, copy, modify, and distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.\\n\\nTHE SOFTWARE IS PROVIDED \\\"AS IS\\\" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\\n\\n" }

  s.source_files        = 'src/libsodium/**/*.{c,h,data}'
  s.prepare_command     = 'sh ./configure --disable-dependency-tracking\n'
  s.requires_arc        = false
  s.header_mappings_dir = 'src/libsodium/include'
  s.compiler_flags      = '-DNATIVE_LITTLE_ENDIAN=1 -DHAVE_MADVISE -DHAVE_MMAP -DHAVE_MPROTECT -DHAVE_POSIX_MEMALIGN -DHAVE_WEAK_SYMBOLS'
end
